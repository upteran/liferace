var gulp = require('gulp'),
    postcss = require('gulp-postcss'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    watch = require('gulp-watch'),
    imagemin = require('gulp-imagemin'),
    spritesmith = require('gulp.spritesmith'),
    pxtorem = require('postcss-pxtorem'),
    cssnext = require("postcss-cssnext"),
    importt = require("postcss-import"),
    urls = require("postcss-url"),
    plumber = require('gulp-plumber'),
    csscomb = require('gulp-csscomb'),
    precss = require('precss'),
    flexbugs = require('postcss-flexbugs-fixes'),
    rucksack = require('rucksack-css'),
    rigger = require('gulp-rigger'),
    initialprop = require('postcss-initial'),
    htmlreplace = require('gulp-html-replace'),
    uglify = require('gulp-uglify');
    cssnano = require('gulp-cssnano'),
    runSequence = require('run-sequence');



gulp.task('html:build', function () {
    gulp.src('verstka-source/**/*.html')
        .pipe(rigger())
        .pipe(gulp.dest('verstka/'))
        // .pipe(reload({stream: true}));
});
gulp.task('js:build', function () {
gulp.src('verstka/js/plugins.js') //Найдем наш main файл
        .pipe(rigger()) 
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write()) //Пропишем карты
        .pipe(gulp.dest('verstka/js/build')) //Выплюнем готовый файл в build
        .pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('serve', function() {
  browserSync({
    server: {
      baseDir: ['verstka']
    },
    reloadDelay: 300
  });

  //gulp.watch(['./**/*.html', './res/**/*.css', './js/**/*.js'], {cwd: 'verstka'}, reload);
});


gulp.task('sprite', function () {
  var spriteData = gulp.src('verstka-source/res/images/spr/*.png').pipe(spritesmith({
    imgName: 'sprite.png',
    cssName: 'sprite.css',
    padding: 2,
    cssTemplate: 'mustacheSprite.css.mustache',
    sourcemap: true
  }));

  spriteData.img
    // .pipe(imagemin())
    .pipe(gulp.dest('verstka/res/images'));

  spriteData.css
    // .pipe(csso())
    .pipe(gulp.dest('verstka-source/res/css/parts'));
});

  gulp.task('style', function () {
    var processors = [
        precss,
        rucksack,
         cssnext({
              features: {
                  autoprefixer: {
                     browsers: ['last 7 version']
                  },
                    sourcemap: true,
                    rem : false
              }
          }),
         
        flexbugs,
        initialprop,
        pxtorem({
            replace: false,
            prop_white_list: ['font', 'font-size', 'line-height', 'letter-spacing']
        })  
        
        ];

    return gulp.src('verstka-source/res/css/style.css')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(postcss(processors))
        //.pipe(csscomb())
        .pipe(cssnano())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('verstka/res/css'))
        //.pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('watch', function(){
    //gulp.watch('verstka-source/res/css/**/*.css', ['style']);
    gulp.watch('verstka-source/res/css/**/*.css', function(){runSequence('style', reload)});
    //gulp.watch('verstka-source/**/*.html', ['html:build']);
    gulp.watch('verstka-source/**/*.html', function(){runSequence('html:build', reload)});
  });

gulp.task('default', ['watch', 'style', 'html:build', 'js:build', 'serve']);
