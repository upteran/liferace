var user_uuid;
var hash;
var push_close = true,
    push_menu = $('.push-menu'),
    close_btn = $('.close-push'),
    fader = $('.fader'),
    push_open = $('.b-push-btn');

// NEW SCRIPTS
jQuery(document).ready(function ($) {
	var fader = $('.fader'),
		wrapper = $('.wrapper');
	var key = getQueryVariable("key");
	var inputKey = $("#input-key");
	if(inputKey)
	{
	    inputKey.val(key);
	}

	cardHover();

    (function () {
        // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
        if (!String.prototype.trim) {
            (function () {
                // Make sure we trim BOM and NBSP
                var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                String.prototype.trim = function () {
                    return this.replace(rtrim, '');
                };
            })();
        }
        [].slice.call(document.querySelectorAll('.cool-input__field')).forEach(function (inputEl) {
            // in case the input is already filled..
            if (inputEl.value.trim() !== '') {
                classie.add(inputEl.parentNode, 'cool-input--filled');
            }

            // events:
            inputEl.addEventListener('focus', onInputFocus);
            inputEl.addEventListener('blur', onInputBlur);
        });

        function onInputFocus(ev) {
            classie.add(ev.target.parentNode, 'cool-input--filled');
        }

        function onInputBlur(ev) {
            if (ev.target.value.trim() === '') {
                classie.remove(ev.target.parentNode, 'cool-input--filled');
            }
        }
    })();

	// close-popup
    $('.popup__close, .popup__close2, .fader').click(function () {
        closePopup();
    });

    //launcher
    $('#downloadTrigger').click(function () {
        location.href = '/api/download/' + hash + '/launcher-setup.exe';
    });

    /*qa / accordion */

    $('.qa__core').hide();

    $('.qa__header').click(function(){
        $(this).parents('.qa__item').toggleClass('qa__opened').find('.qa__core').stop().slideToggle();
        $(this).find('.toggler').toggleClass('minus');
    });


    // close current page
    $(document).on('click', '#closePage', function  () {
        close();
        location.href = 'http://liferace.net/';
    });

    // push-menu
    $(document).on('click' , function (e) {
        var _target = e.target;
        if(push_menu.hasClass('push-menu--open') && e.target.closest('.push-menu') == null && e.target.closest('.push-btn') == null) {
            closePushMenu();
        }
    });

    
    // push-menu btn click
    $(document).on('click', '.push-btn', function () {
        var _this = $(this);
        if(push_close){
            openPushMenu();
        } else {
            closePushMenu();
        }
    });


	// tabs 
	$(document).on('click', '.tabs__list-item', function(){
		var tab = $(this),
			sibl_tabs = tab.siblings('li'),
			index = tab.index(),
			tab_wrap = tab.closest('.tabs');

				tab.addClass('active')
				   .siblings('li')
				   .removeClass('active');

				tab_wrap.find('.tabs__content')
						.eq(index)
						.addClass('active')
						.siblings('.tabs__content')
						.removeClass('active');
	});

	//parallax
	var parallaxStart = function(){
		var winW = window.innerWidth;
		var parallax01 = $('.jarallax_01'),
			parallax02 = $('.jarallax_02'),
			parallax03 = $('.jarallax_03');
		if(winW >= 1360){
			parallax01.jarallax({
				"speed": 0.10,
				"imgSrc": 'res/images/bg/info-sec01.jpg'
			});
			parallax02.jarallax({
				"type": "scroll-opacity",
				"imgSrc": 'res/images/bg/info-sec02.jpg'
			});
			parallax03.jarallax({
				"speed": 0.10,
				"imgSrc": 'res/images/bg/info-sec03.jpg'
			});
		} else if(winW <= 1360 && winW >= 1100){
			parallax01.jarallax({
				"speed": 0.10,
				"imgSrc": 'res/images/bg/info-sec01-md.jpg'
			});
			parallax02.jarallax({
				"type": "scroll-opacity",
				"imgSrc": 'res/images/bg/info-sec02-md.jpg'
			});
			parallax03.jarallax({
				"speed": 0.10,
				"imgSrc": 'res/images/bg/info-sec03-md.jpg'
			});
		} else {
			parallax01.jarallax('destroy');
			parallax02.jarallax('destroy');
			parallax03.jarallax('destroy');
		}
	};
	parallaxStart();

	// news cards slider
	var mainScale = function(){
	var corrX = window.innerWidth;
	if(corrX <= 767){
			$( '.cycle' ).slick({
				slidesToShow: 1,
				arrows: false,
				dots: true,
				dotsClass: 'cycle-pager',
				infinite: false

			});
		}else{
			return;
		};
	};
	mainScale();
	window.onresize = function (event)
	{
		//mainScale();
		parallaxStart();
	};
	
	// screenshots modal-win open
	$('.js-modal').fancybox({
		padding: 0,
		autoSize: false,
		maxWidth: '90%',
		tpl: {
	        closeBtn: '<a title="Закрыть" class="fancybox-item fancybox-close" href="javascript:;"></a>'
	    }
	});
	
	//colorbox , video modal win
	var cboxOptions = {
	  iframe:true,
	  width: '90%',
	  height: '40%',
	  maxWidth: '640px'
	}

	$(window).resize(function(){
	    $.colorbox.resize({
	      width: window.innerWidth > parseInt(cboxOptions.maxWidth) ? cboxOptions.maxWidth : cboxOptions.width,
	      height: window.innerHeight > parseInt(cboxOptions.maxHeight) ? cboxOptions.maxHeight : cboxOptions.height
	    });
	});

	$('a[rel="video-group"], .js-modal_video').colorbox(cboxOptions);


	// scroll down
    $('.down a').on('click', function(e){
        e.preventDefault();
          var elementClick = $(this).attr("href"),
        	  destination = $(elementClick).offset().top;
        	  $('html,body').stop().animate({ scrollTop: destination - 10}, 1000);
    });

	// call popup
	$(document).on('click', '.js-popup', function(e){
		$(document).find('.popup.visible')
				   .removeClass('visible shown')
				   .addClass('out');
		fader.removeClass('open');
		
		e.preventDefault();
		var $this = $(this),
			popup_name = $this.attr('data-name'),
			cur_popup = $(document).find('#' + popup_name);
				cur_popup.addClass('visible')
						 .addClass('shown')
						 .removeClass('invisible')
						 .removeClass('out');
 					closePushMenu();
 					wrapper.addClass('minimized');
				fader.addClass('open');

				var winW = $(window).width();
				if(winW < 500 || cur_popup.hasClass('popup_large')){
					$("body, html").animate({"scrollTop":0}, 100);
				}
				cur_popup.find('input').first().focus();
	});

	//change pass popup
	$('#resetPassForm button[type="submit"]').on('click', function(){
		$(this).closest('.popup__block')
			   .siblings('.msg-success-frame')
			   .addClass('active');
	});
	// open hid feedback
	var height_feed = $('.feedback').height();
	$('.callFeedback').on('click', function () {
		var curr_b = $('.hid-feedback')
		curr_b.toggleClass('open')
			  .slideToggle(340)
	});
	$('.hid-feedback .close').on('click', function () {
		var curr_b = $('.hid-feedback')
		curr_b.toggleClass('open')
			  .slideToggle(340)
	});

	//input focus on feedback page
	var feedback_form = document.getElementById('feedback');
		if(feedback_form !== null){
			feedback_form.querySelector('input')
						 .focus();
		}
	//feedback animation 
	$('#feedSuccessTrigger').on('click', function(){
		$(this).closest('.feedback').addClass('hidden');
		$(this).closest('.hid-feedback').find('.msg-success-feedback').addClass('active');
	});
	//custom input file
	var wrapper_upload = $( "#upload-mask" ),
        inp = wrapper_upload.find( "input" ),
        text = wrapper_upload.find( ".upload__text" ),
        btn = wrapper_upload.find( "button" ),
        lbl = wrapper_upload.find( "div" );
    // Crutches for the :focus style:
    inp.focus(function(){
        wrapper_upload.addClass( "focus" );
    }).blur(function(){
        wrapper_upload.removeClass( "focus" );
    });
    //copy input file name
    var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;
    inp.change(function(){
        var file_name;
        if( file_api && inp[ 0 ].files[ 0 ] )
            file_name = inp[ 0 ].files[ 0 ].name;
        else
            file_name = inp.val().replace( "C:\\fakepath\\", '' );
        if( ! file_name.length ){
            return;
        };
        text.text( file_name );
    }).change();

	    $( window ).resize(function(){
		    $( "#upload-mask input" ).triggerHandler( "change" );
		    cardHover();
		});	

	//custom input file photo
	var wrapper_upload2 = $( "#upload-mask2" ),
        inp2 = wrapper_upload2.find( "input" ),
        text2 = wrapper_upload2.find( "span" ),
        btn2 = wrapper_upload2.find( "button" ),
        lbl2 = wrapper_upload2.find( "div" );
    // Crutches for the :focus style:
    inp2.focus(function(){
        wrapper_upload2.addClass( "focus" );
    }).blur(function(){
        wrapper_upload2.removeClass( "focus" );
    });
    //copy input file name
    
    inp2.change(function(){
    	var file_api2 = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;
        var file_name2;
        if( file_api2 && inp2[ 0 ].files[ 0 ] )
            file_name2 = inp2[ 0 ].files[ 0 ].name;
        else
            file_name2 = inp2.val().replace( "C:\\fakepath\\", '' );
        if( ! file_name2.length ){
            return;
        };
        text2.text( file_name2 );
    }).change();

	    $( window ).resize(function(){
		    $( "#upload-mask2 input" ).triggerHandler( "change" );
		});


	//личный кабинет
	$('.email-edit-btn').on('click', function () {
		$(this).addClass('hidd');
		var hidd_b = $(this).closest('.form-group').siblings('.edit-hidd');
		if(hidd_b.hasClass('hidd')){
			hidd_b.removeClass('hidd');
		} else {
			hidd_b.addClass('hidd');
		}
	});



	$(document).on('click', '.acc__submit', function(){
		var $this = $(this),
			confirm_type = $this.attr('data-type'),
			error_msg = $this.closest('.input-group').siblings('.input_msg');
		var inp = $this.siblings('input'),
			inp_val = inp.val();
		var pattern_email = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
	    if($.trim(inp.val()) === ''){
	           error = true;
	           error_msg.show()
	                     .text('Пожалуйста, заполните поле')
	                     .addClass('input-msg-error')
	                     .removeClass('input-msg--lg');
	            inp.addClass('input_error');
	       } else {
	           error_msg.removeClass('input-msg-error');
	            inp.removeClass('input_error');
	       }
	       // email valid
	       switch(confirm_type) {
	       	case 'email':
	       		var post_url = 'email_url';
	        	if(inp_val !== '' && inp_val !== undefined){
			        if(pattern_email.test(inp_val))
			        {
			            error_msg.removeClass('input-msg-error');
			            inp.removeClass('input_error');
						// ajax запрос на сервер с новой почтой
						$.ajax({
		                     type: "POST",
		                     url: post_url,
		                     dataType: "json",
		                     data: inp_val,
		                     success: function(data){
		                        console.log(inp_val);
		                     },
		                     error: function(data){
		                        console.log(data.status);
		                     }
		                 });
						console.log('ajax-email-edit');
						openPopup($('#emailChange'));
						// $("body, html").animate({"scrollTop":0}, 100);
			        }
			        else
			        {
			        	console.log('error_email');
			            error_email = true;
			            error_msg.show().text("Почта введена некорректно.").addClass('input-msg-error');
			            inp.addClass('input_error');
			        }
			    };
	       		break;
	       	case 'pass':
	       		var post_url = 'pass_url';
	       		if(inp_val !== '' && inp_val !== undefined){
		            if(inp_val.length>=3)
		            {
		                error_msg.removeClass('input-msg-error');
		                inp.removeClass('input_error');
	                    console.log('ajax-pass-edit');
						// ajax запрос на сервер с новым паролем
						//
						$.ajax({
		                     type: "POST",
		                     url: post_url,
		                     dataType: "json",
		                     data: inp_val,
		                     success: function(data){
		                        console.log(inp_val);
		                     },
		                     error: function(data){
		                        console.log(data.status);
		                     }
		                 });
						openPopup($('#changePass'));
						// $("body, html").animate({"scrollTop":0}, 100);
		            }
		            else
		            {
		                error_pass = true;
		                error_msg.show()
		                           .text("Пароль должен содержать не менее 3-х символов.")
		                           .addClass('input-msg-error');
		                inp.addClass('input_error');
		            }
		        }
	       		break;
	       	case 'key':
	       		var post_url = 'key_url';
			        if (!inp_val) {
			            return;
			        } else {
			        	//Запрос на сервер с ключом альфа-тестера
			        	$.ajax({
		                     type: "POST",
		                     url: post_url,
		                     dataType: "json",
		                     data: inp_val,
		                     success: function(data){
		                     	error_msg.removeClass('input-msg-error');
		                     	inp.removeClass('input_error');
		                     	openPopup($('#activatePopup'));
		                     	// $("body, html").animate({"scrollTop":0}, 100);
		                        console.log(inp_val);
		                     },
		                     error: function(err){
		                        console.log(err.status);
		                        	if (err.status === 404) { // меняем код ошибки на нужный
				                    	console.log('ошибочка');
				                    	error_msg.show()
				                    			 .text("Введен неверный ключ. Попробуйте еще раз.")
				                    			 .addClass('input-msg-error');
				                    	inp.addClass('input_error');
				                	}
		                     }
		                 });
			        }
	       };
	});

	// active mail spam
	$(document).ready(function(){
		var sub_checkbox = $('#subs'),
			wrapp = sub_checkbox.closest('.form-group'),
			sub_checkbox_text = 'Не активирована';
			sub_checkbox.change(function(){
				if(!(sub_checkbox.prop('checked'))){
					sub_checkbox.siblings('label')
								.text(sub_checkbox_text);
					wrapp.addClass('disactive');
				} else {
					sub_checkbox.siblings('label')
								.removeClass('input-msg-error')
								.text('Активирована');
					wrapp.removeClass('disactive');
				}
			}).change();

	});



	//roadmap page
	//roadmap progress bar 
	rendProgressBar();
	
	//fun
	function rendProgressBar(){
		var progr_bar = $(document).find('.progress-bar__success');
			progr_bar.each(function(){
				var prog_bar_width = $(this).attr('data-width'),
					$this = $(this);
				$this.animate({
					   width: prog_bar_width + '%'
				}, 1100);
				animateNum($this, prog_bar_width, 900); // animate num
			});
			
	};
	
	function animateNum(el, valueEnd, duration) {
	    $({
	        someValue: 0
	    }).stop().animate({
	        someValue: valueEnd
	    }, {
	        duration: duration,
	        easing: "swing",
	        step: function() {
	            el.text(commaSeparateNumber(Math.round(this.someValue)) + "%")
	        }
	    })
	}

	function commaSeparateNumber(e) {
	    for (;
	        /(\d+)(\d{3})/.test(e.toString());) e = e.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	    return e
	}

	function closePushMenu(){
	    push_menu.removeClass('push-menu--open');
	    close_btn.removeClass('close-push--click');
	    push_open.removeClass('vis');
	    push_close = true;
	};

	function openPushMenu(){
	    push_menu.addClass('push-menu--open');
	    close_btn.addClass('close-push--click');
	    push_open.addClass('vis');
	    push_close = false;
	};

    function closePopup(){
    	$(document).find('.popup.visible')
        		   .removeClass('visible')
        		   .removeClass('shown')
        		   .removeClass('out')
        		   .addClass('invisible');
    	fader.removeClass('open');
    	wrapper.removeClass('minimized');
    };

    function openPopup(el){
    	fader.addClass('open');
	    el.addClass('visible')
		  .addClass('shown')
		  .removeClass('invisible')
		  .removeClass('out');
    }
    function toggleFeedback(formName){
    	var form = formName;
			form.toggleClass('open').slideToggle();
			form.find('input')[0].focus();
    }

	function getQueryVariable(variable) {
	  var query = window.location.search.substring(1);
	  var vars = query.split("&");
	  for (var i=0;i<vars.length;i++) {
	    var pair = vars[i].split("=");
	    if (pair[0] == variable) {
	      return pair[1];
	    }
	  } 
	  //alert('Query Variable ' + variable + ' not found');
	}
	function cardHover(){
		var winW = window.innerWidth;
		if(winW > 1024){
			$('.card-item').hover(
				function(){
					var text_h = $(this).find('.card-item__descript').height();
					var text = $(this).find('.card-item__descript');
					var card_h = $(this).height();
					text.stop().animate({'bottom': '' + (card_h - text_h - 60)}, 350);
				},
				function(){
					var text = $(this).find('.card-item__descript');
					text.stop().animate({'bottom': 0}, 350);
				}
			);
		}		
	}

});


