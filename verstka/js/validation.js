var user_uuid;
var hash;

jQuery(document).ready(function ($) {

    //анкета пользователя
    $('#regTrigger').click(function (ev) {
        console.log('regTrigger');
        if (!$('input[name=age]:checked', '#question').val() || !$('input[name=frequency]:checked', '#question').val()) {
            $('#error-question').show();
            return;
        } else {
            $('#error-question').hide();
        }

        var data = {};
        $('#question').serializeArray().forEach(function (item) {
            data[item.name] = item.value;
        });

        ev.target.disabled = true;
        $.post('/api/question', data)
            .done(function (data) {
                console.log("done question");
                $("body, html").animate({"scrollTop":0},"slow");
                $('#questPopup').removeClass('visible').removeClass('shown').addClass('out');
                $('#regPopup').addClass('visible').addClass('shown').removeClass('invisible').removeClass('out');
                $('#question')[0].reset();

                user_uuid = data;
                ev.target.disabled = false;
            })
            .fail(function (err) {
                console.log("error question");
                console.error(err);
                ev.target.disabled = false;
            });
    });


    // NEW SCRIPTS

    // validate forms
    $('.button--submit').click(function(e){
        var _this = $(this),
            _form = _this.closest('form'),
            error = false,
            error_name = false,
            error_email = false,
            error_pass = false,
            form_type = _this.attr('data-name');
            var error_msg_name,error_msg_email,
            	empty_text = 'Пожалуйста, заполните поле';
            var pattern_email = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
        var data = {};
        // определяем с какой формы идет запрос
        switch(form_type) {
        	case 'login': //форма входа
        		post_url = '/api/login';
        		break;
        	case 'createAccount':   //форма создания аккаунта
         		var reg = /^[0-9._a-zA-Z-]+$/,
        			regFirsLetter = /^[a-zA-Z]+$/,
    				regFirsLastLetter = /^[0-9a-zA-Z]+$/,
    				addClassName = 'input-msg-error input-msg--lg';
    				
        		post_url = '/api/register';
        		error_msg_name = 'Недопустимый логин. Логин может состоять из латинских символов, цифр, одинарного дефиса или точки. Он должен начинаться с буквы, заканчиваться буквой или цифрой и содержать не более 15 символов.'
        		break;
        	case 'resetPass':     //форма сброса пароля
        		post_url = '/api/reset';
        		break;
        	case 'feedback':  //форма обратной связи
        		var reg = /^[а-яА-ЯёЁa-zA-Z._0-9]+$/,
        			regFirsLetter = /^[а-яА-ЯёЁa-zA-Z]+$/,
    				regFirsLastLetter = /^[а-яА-ЯёЁa-zA-Z0-9]+$/,
    				addClassName = 'input-msg-error';
        		
                post_url = '/api/feedback';
        		error_msg_name = 'Вы забыли представиться';
        		break;
        }
        _form.serializeArray().forEach(function(item){
            if (item.name === 'password') {
                data.hashPassword = CryptoJS.SHA512(item.value).toString();
            } else {
                data[item.name] = item.value;
            }
        });
        data.user_uuid = user_uuid;

        var arrayInputs = _form.find('.input');
        arrayInputs.each(function(item){
            var _error_msg =  $(this).closest('.cool-input__container').find('.cool-input__msg');
            if($.trim($(this).val()) === ''){
                error = true;
                _error_msg.show()
                          .text(empty_text)
                          .addClass('input-msg-error')
                          .removeClass('input-msg--lg');
                $(this).addClass('input_error');
            } else {
                _error_msg.removeClass('input-msg-error');
                $(this).removeClass('input_error');
            }
        });


        //valid email
        var _email = _form.find('.input-email'),
        	_email_val = _email.val(),
        	_email_error = _email.closest('.cool-input__container').find('.cool-input__msg');
        if(_email_val !== '' && _email_val !== undefined){
            if(pattern_email.test(_email_val))
            {
                //error = false;
                error_email = false;
                _email_error.removeClass('input-msg-error');
            }
            else
            {
            	console.log('error_email');
                error_email = true;
                _email_error.show().text("Пожалуйста, укажите корректный адрес своей электронной почты.").addClass('input-msg-error');
            }
        }
       	// valid name 
        var _name = _form.find('.input-name'),
        	_name_val = _name.val(),
        	_name_error = _name.closest('.cool-input__container').find('.cool-input__msg');
        if	(_name_val !== '' && _name_val !== undefined){
            if((_name_val.match(reg)) && _name_val.charAt(0).match(regFirsLetter) && _name_val.substr(_name_val.length - 1).match(regFirsLastLetter) && _name_val.length<15 && _name_val.length>2)
            {
                //error = false;
                error_name = false;
                _name_error.removeClass('input-msg-error input-msg--lg');
            }
            else
            {
                error_name = true;
                _name_error.show()
                           .text(error_msg_name)
                           .addClass(addClassName);
            }
        }

        console.log(error);
        //valid checkbox
        var accept = _form.find("#accept");
        if(accept.length !== 0){
        	if (!accept.is(":checked")) {
	            error = true;
	            classie.add(accept[0].parentNode, 'input_error');
	        } else {
	        	//error = false;
	            classie.remove(accept[0].parentNode, 'input_error');
	        }

	        if (error) {
	            return;
	        }
        }
       	//validate pass
        var _pass = _form.find('.input-password'),
            _pass_val = _pass.val(),
            _pass_error = _pass.closest('.cool-input__container').find('.cool-input__msg');
        if(_pass_val !== '' && _pass_val !== undefined){

            if(_pass_val.length>=3)
            {
            	//error = false;
            	error_pass = false;
                _pass_error.removeClass('input-msg-error');
            }
            else
            {
                error_pass = true;
                _pass_error.show()
                           .text("Пароль должен содержать не менее 3-х символов.")
                           .addClass('input-msg-error');
            }
        }

        console.log(post_url);
        console.log(data);

        var _popup_wrap_reg = _this.closest('#regPopup'),
            _popup_reset_pass = _this.closest('#resetPass');
        // check errors on server side
        if(error || error_name || error_pass || error_email){
        	return;
        } else {
        	e.target.disabled = true;
	        $.post(post_url, data)
	            .done(function () {
	                //console.log(data);
	                if(_popup_wrap_reg){
	                    _popup_wrap_reg.removeClass('visible').removeClass('shown').addClass('out');
	                    $('#regSuccess').addClass('visible').addClass('shown').removeClass('invisible').removeClass('out');
	                }
                    if(_popup_reset_pass){
                        _popup_reset_pass.find('.popup__msg-success').addClass('visible');
                    }
	                // remove errors
	                arrayInputs.each(function (item) {
	                     var _error_msg =  $(this).closest('.cool-input__container').find('.cool-input__msg');
	                    _error_msg.removeClass('input-msg-error');
	                });
	                classie.remove(accept[0].parentNode, 'input_error');

	                _form.reset();
	                e.target.disabled = true;
	            })
	            .fail(function (err) {
	                console.log(err);
                    if(_popup_reset_pass){
                        _popup_reset_pass.find('.popup__msg-success').addClass('visible');
                    }
	                 var _error_msg =  _email.closest('.cool-input__container').find('.cool-input__msg');
	                if (err.status === 400) {
	                    if (err.responseText === 'INVALID_EMAIL') {
	                        _error_msg.show().text('Почта введена некорректно.');
	                    } else if (err.responseText === 'EMAIL_EXISTS') {
	                        _error_msg.show().text('Такая почта уже зарегистрирована.');
	                    } else {
	                        _error_msg.show().text('Пожалуйста, заполните поле.');
	                    }
	                }

	                //console.error(err);
	                e.target.disabled = false;
	            });
        }


    });
});
	//valid end